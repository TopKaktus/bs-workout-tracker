<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Exercise;
use \App\User;

class exerciseController extends Controller
{
    public function index()
    {
        // TODO: add validation
        return view('exercises.index');

    }

    public function show(Exercise $exercise)
    {
        // TODO: add validation
        return view('exercises.show', compact('exercise'));

    }

    public function create()
    {

        return view('exercises.create');

    }


    public function store()
    {
        // TODO: implement Validation - after you are sure it's working create a custom request
        // https://laravel.com/docs/5.7/validation#form-request-validation Creating Form Requests
        // TODO: implement firstOrCreate or disable creation of the same Exercise

        Exercise::create([
            'user_id' => \Auth::id(),
            'name' => request('name'),
        ]);


        return redirect('/exercises');
    }


    public function edit(Exercise $exercise)
    {

        return view('exercises.edit', compact('exercise'));

    }

    public function update(Exercise $exercise)
    {
        // TODO: add validation
        $exercise->update(request(['name']));

        return redirect('/exercises');

    }

    public function destroy(Exercise $exercise)
    {
        // TODO: add validation
        $exercise->delete();

        return redirect('/exercises');
    }
}

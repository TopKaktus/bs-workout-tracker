<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exercise_id',
        'sets',
        'reps',
        'note'
    ];

    // workout belongs to exercise
    public function exercise()
    {
        return $this->belongsTo(Exercise::class);
    }

    // get workouts for user
    public function scopeUserExercises($query)
    {
        return $query->whereHas('exercise', function ($query) {
            $query->where('user_id', auth()->user()->id);
        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
    ];

    // exercise belongs to user
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    // exercise has many workouts
    public function workouts()
    {
        return $this->hasMany(Workout::class);
    }

    public function addWorkout($workout)
    {

        $this->workouts()->create($workout);

    }

}

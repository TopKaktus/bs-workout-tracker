@if ($errors->any())
    <div class="col-lg-12">
        <div class="mt-3 alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach
            </ul>
        </div>
    </div>
@endif
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12 text-center mt-4 mb-4">
            <h1>Exercise</h1>
        </div>

        <div class="col-md-12 text-center">

            <p class="lead">{{ $exercise->name }}</p>

            <p>
                <a href="/exercises/{{$exercise->id}}/edit" class="btn btn-link">Edit</a>
            </p>

        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 text-center mt-4 mb-4">
            <h1>Exercises</h1>
        </div>

        <div class="col-md-12 text-center mt-4 mb-4">
            <ul class="list-group">
                @foreach (Auth::user()->exercises as $exercise)
                    <li class="list-group-item">
                        <a href="/exercises/{{$exercise->id}}">
                            {{ $exercise->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

        <a href="/exercises/create" class="btn btn-danger">Add exercise</a>
    </div>
</div>
@endsection

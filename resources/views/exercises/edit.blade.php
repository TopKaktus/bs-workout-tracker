@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12 text-center mt-4 mb-4">
            <h1>Edit exercise</h1>
        </div>

        <div class="col-md-8 text-center mt-4 mb-4">

            <form method="POST" action="/exercises/{{$exercise->id}}" class="text-center d-block">
                @csrf

                @method('PATCH')

                <div class="form-group">
                    <label for="name" class="d-block mb-4"></label>
                    <input type="name" value="{{ $exercise->name }}" id="name" name="name" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Update</button>

            </form>

        </div>


        <div class="col-md-8 text-center mt-4 mb-4">

            <form method="POST" action="/exercises/{{$exercise->id}}" class="d-block">
                @csrf

                @method('DELETE')

                <button type="submit" class="btn btn-link">Delete exercise</button>
            </form>

        </div>

    </div>
</div>
@endsection

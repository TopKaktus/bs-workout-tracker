@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 text-center mt-4 mb-4">
            <h1>Your Workouts</h1>
        </div>

        <div class="col-md-12 text-center mt-4 mb-4">
            <ul class="list-group">
                @foreach (\App\Workout::userexercises()->get() as $workout)

                    <li class="list-group-item">
                        <b>Exercise:</b> {{ $workout->exercise->name }},
                        <br>
                        <br>Sets: {{ $workout->sets }},
                        <br>Reps: {{ $workout->reps }},
                        <br>Note: {{ $workout->note }},
                        <br>Date: {{ $workout->created_at->format('d/m/Y') }}
                    </li>

                @endforeach
            </ul>
        </div>

        <a href="/workouts/create" class="btn btn-danger">Add workout</a>

    </div>
</div>
@endsection

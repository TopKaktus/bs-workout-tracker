@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 text-center mt-4 mb-4">
            <h1 class="title text-center mb-5">Add workout</h1>
        </div>

        <div class="col-md-12">
            <form method="POST" action="/workouts">
                @csrf

                <div class="input-group mb-4">
                    <select class="custom-select" id="inputGroupSelect01" name="exercise">
                        @foreach (Auth::user()->exercises as $exercise)
                            <option value="{{ $exercise->id }}" {{ (old("exercise") == $exercise->id ? "selected":"") }}>{{ $exercise->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text">Sets</span>
                    <input type="text" class="form-control" name="sets" id="sets" value="{{ old('sets') }}">
                    <span class="input-group-text">Reps</span>
                    <input type="text" class="form-control" name="reps" id="reps" value="{{ old('reps') }}">
                </div>

                <div class="form-group">
                    <label for="note">Note</label>
                    <textarea class="form-control" name="note" id="note"  rows=4" value="{{ old('note') }}"></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

        @include('partials.errors')
    </div>
</div>

@endsection
